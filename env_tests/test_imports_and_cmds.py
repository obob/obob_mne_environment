import subprocess
import sys
import os

from nose.tools import assert_equal

def test_imports():
    import mne
    import obob_condor
    import fieldtrip2mne
    import obob_mne
    import vtk
    import mayavi.core.ui.mayavi_scene
    import eelbrain
    import autoreject

def test_mne_cmd():
    bin_folder = os.path.dirname(sys.executable)
    results = subprocess.run([os.path.join(bin_folder, 'mne')], stdout=subprocess.PIPE,
                             stderr=subprocess.STDOUT, shell=True)
    assert_equal(results.returncode, 0)

    stdout_lines = results.stdout.decode('utf-8').splitlines()

    assert_equal(stdout_lines[0], 'Usage : mne command options')
    assert_equal(stdout_lines[-1], 'Getting help example : mne compute_proj_eog -h')

def test_obob_mne_cmds():
    bin_folder = os.path.dirname(sys.executable)

    results = subprocess.run([os.path.join(bin_folder, 'import_subject_to_freesurfer')],
                             stdout=subprocess.PIPE,
                             stderr=subprocess.STDOUT, shell=True)
    assert_equal(results.returncode, 1)

    stdout_lines = results.stdout.decode('utf-8').splitlines()
    assert_equal(stdout_lines[0], 'Traceback (most recent call last):')
    assert_equal(stdout_lines[-1], 'IndexError: list index out of range')

    results = subprocess.run(
        [os.path.join(bin_folder, 'make_freesurfer_bem')],
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT, shell=True)
    assert_equal(results.returncode, 1)

    stdout_lines = results.stdout.decode('utf-8').splitlines()
    assert_equal(stdout_lines[0], 'Traceback (most recent call last):')
    assert_equal(stdout_lines[-1], 'IndexError: list index out of range')

    results = subprocess.run(
        [os.path.join(bin_folder, 'make_freesurfer_sourcespaces')],
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT, shell=True)
    assert_equal(results.returncode, 1)

    stdout_lines = results.stdout.decode('utf-8').splitlines()
    assert_equal(stdout_lines[0], 'Traceback (most recent call last):')
    assert_equal(stdout_lines[-1], 'IndexError: list index out of range')