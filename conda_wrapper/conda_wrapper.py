import subprocess
import io
import os
import inspect

def _get_python_bin_for_env(env):
    if os.path.isdir(env):
        return os.path.join(env, 'bin', 'python')

    subprocess_list = ['conda', 'env', 'list']
    results = subprocess.run(subprocess_list, stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE)

    for cur_line in results.stdout.decode('utf-8').splitlines():
        if cur_line.startswith('#'):
            continue
        try:
            cur_line_split = cur_line.split()
            if cur_line_split[0] == env:
                return os.path.join(cur_line_split[-1], 'bin', 'python')
        except IndexError:
            pass

def _run_and_show(subprocess_list):
    with subprocess.Popen(subprocess_list, stdout=subprocess.PIPE,
                          stderr=subprocess.STDOUT,
                          universal_newlines=True, bufsize=0) as conda_proc, \
         io.StringIO() as buf:

        for line in conda_proc.stdout:
            print(line, end='')
            buf.write(line)
        output = buf.getvalue()
    rc = conda_proc.returncode

    return (rc, output)

def _run_in_condaenv(subprocess_list, env):
    subprocess_list.insert(0, _get_python_bin_for_env(env))
    subprocess_list.insert(1, '-m')

    return subprocess_list


def get_all_envs():
    all_envs = []
    subprocess_list = ['conda', 'env', 'list']

    results = subprocess.run(subprocess_list, stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE)

    for cur_line in results.stdout.decode('utf-8').splitlines():
        if cur_line.startswith('#'):
            continue
        try:
            cur_line_split = cur_line.split()
            all_envs.append(cur_line_split[0])

            if len(cur_line_split) == 3:
                all_envs.append(cur_line_split[2])
            elif len(cur_line_split) == 2:
                all_envs.append(cur_line_split[1])
        except IndexError:
            pass

    return all_envs

def has_env(env_name):
    return env_name in get_all_envs()

def _create_update_env(create, env_name, env_file, force_mne_from_pip=True, force_pysurfer_from_pip=True):
    subprocess_list = ['conda', 'env']
    if create:
        subprocess_list.append('create')
    else:
        subprocess_list.append('update')

    subprocess_list.extend(['-f', env_file])
    if env_name[0] == '/':
        subprocess_list.extend(['-p', env_name])
    else:
        subprocess_list.extend(['-n', env_name])

    (rc, output) = _run_and_show(subprocess_list)

    if rc != 0:
        raise RuntimeError('Task aborted with an error:\n%s' % (output, ))

    if force_mne_from_pip:
        (rc, output) = _run_and_show(_run_in_condaenv(['pip', 'install', '--force-reinstall', '-U', 'mne'], env_name))

        if rc != 0:
            raise RuntimeError('Task aborted with an error:\n%s' % (output))

    if force_pysurfer_from_pip:
        (rc, output) = _run_and_show(_run_in_condaenv(['pip', 'install', '--force-reinstall', '-U', '--no-deps', 'pysurfer'], env_name))

        if rc != 0:
            raise RuntimeError('Task aborted with an error:\n%s' % (output))


def create_env(env_name, env_file, force_mne_from_pip=True):
    return _create_update_env(True, env_name=env_name, env_file=env_file, force_mne_from_pip=force_mne_from_pip)

def update_env(env_name, env_file, force_mne_from_pip=True):
    return _create_update_env(False, env_name=env_name, env_file=env_file, force_mne_from_pip=force_mne_from_pip)

def run_nosetests_in_env(env_name):
    this_folder = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
    (rc, output) = _run_and_show(_run_in_condaenv(['nose', '-w', os.path.join(this_folder, '..', 'env_tests')], env_name))
