import requests
import re
import packaging.version
import tempfile
import argparse
import conda_wrapper
import inspect
import os

import yaml

MNE_ENV_URL = 'https://raw.githubusercontent.com/mne-tools/mne-python/master/environment.yml'
OBOB_MNE_ENV_FNAME = 'env_yml/obob_mne_environment.yml'

parse_version_regex = r'^(?P<pkg>[^>=<]*)(?P<cmp>[>=<]*)(?P<v_number>[0-9.]*)'


def parse_deps(deps):
    for idx, item in enumerate(deps):
        if isinstance(item, str):
            deps[idx] = re.match(parse_version_regex, item).groupdict()

def unparse_deps(deps):
    for idx, item in enumerate(deps):
        if 'pkg' in item:
            final_string = item['pkg']
            if item['cmp'] and item['v_number']:
                final_string += item['cmp'] + item['v_number']

            deps[idx] = final_string


def main():
    # setup argparse...
    parser = argparse.ArgumentParser()
    group = parser.add_mutually_exclusive_group()
    group.add_argument('--force-create',
                       action='store_true',
                       help='Force creation of a new environment.'
                            'Abort if the env already exists.')
    group.add_argument('--force-update',
                       action='store_true',
                       help='Force update of an existing environment.'
                            'Abort if the env does not exists.'
                       )
    parser.add_argument('environment',
                        help='The conda environment to create or update.')
    args = parser.parse_args()

    this_folder = os.path.dirname(
        os.path.abspath(inspect.getfile(inspect.currentframe())))


    mne_env = yaml.safe_load(requests.get(MNE_ENV_URL).text)
    with open(os.path.join(this_folder, OBOB_MNE_ENV_FNAME), 'rt') as f:
        obob_mne_env = yaml.safe_load(f)

    parse_deps(mne_env['dependencies'])
    parse_deps(obob_mne_env['dependencies'])

    if 'pip' in obob_mne_env:
        if isinstance(obob_mne_env, dict):
            raise RuntimeError('Unsupported!')

    final_env = dict()
    final_env['name'] = obob_mne_env['name']
    final_env['channels'] = obob_mne_env['channels']
    final_env['dependencies'] = mne_env['dependencies']

    for cur_dep in obob_mne_env['dependencies']:
        if 'v_number' in cur_dep:
            all_final_pkg_names = [x['pkg'] for x in final_env['dependencies'] if 'pkg' in x]
            if not cur_dep['pkg'] in all_final_pkg_names:
                final_env['dependencies'].append(cur_dep)
            else:
                other_dep = [x for x in final_env['dependencies'] if 'pkg' in x
                             and x['pkg'] == cur_dep['pkg']][0]

                cur_version = packaging.version.parse(cur_dep['v_number'])
                other_version = packaging.version.parse(other_dep['v_number'])
                if cur_dep['cmp'] in ['>', '>='] and \
                    other_dep['cmp'] in ['>', '>=']:
                    final_version = max(cur_version, other_version)
                    cur_dep['v_number'] = str(final_version)
                elif other_dep['cmp'] == '':
                    other_dep['cmp'] = cur_dep['cmp']
                    other_dep['v_number'] = cur_dep['v_number']
                else:
                    raise RuntimeError('Unsupported!')

        if 'pip' in cur_dep:
            mne_pip = [x['pip'] for x in mne_env['dependencies'] if 'pip' in x][0]
            obob_pip = cur_dep['pip']
            final_pip = mne_pip + obob_pip
            for cur_final_dep in final_env['dependencies']:
                if 'pip' in cur_final_dep:
                    cur_final_dep['pip'] = final_pip


    unparse_deps(final_env['dependencies'])

    with tempfile.NamedTemporaryFile('wt') as env_file:
        yaml.safe_dump(final_env, env_file)
        new_env = not conda_wrapper.has_env(args.environment)
        if new_env and args.force_update:
            raise RuntimeError('--force-update was chosen although the '
                               'environment does not exist.')
        if not new_env and args.force_create:
            raise RuntimeError('--force-create was chosen although the '
                               'environment already exists.')

        if new_env:
            conda_wrapper.create_env(args.environment, env_file.name)
        else:
            conda_wrapper.update_env(args.environment, env_file.name)

    conda_wrapper.run_nosetests_in_env(args.environment)

if __name__ == '__main__':
    main()